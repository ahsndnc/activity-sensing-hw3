//
//  ViewController.m
//  MicrosoftBandSensing
//
//  Created by Asad Malik on 10/3/16.
//  Copyright (c) 2016 GroupV. All rights reserved.
//

#import "ViewController.h"
#import "MicrosoftBandKit_iOS/MicrosoftBandKit_iOS.h"
#import "CoreBluetooth/CoreBluetooth.h"


@interface ViewController() <MSBClientManagerDelegate>
@property (nonatomic, weak) MSBClient *client;

@end

@implementation ViewController

-(void)clientManager:(MSBClientManager *)cm
    clientDidConnect:(MSBClient *)client {
    // handle connected event


}
-(void)clientManager:(MSBClientManager *)cm
 clientDidDisconnect:(MSBClient *)client {
    // handle disconnected event
}
-(void)clientManager:(MSBClientManager *)cm client:(MSBClient *)client
didFailToConnectWithError:(NSError *)error {
    // handle failure event
   }

- (IBAction)init:(id)sender {
    [[MSBClientManager sharedManager] setDelegate:self];
    NSArray *attachedClients = [[MSBClientManager sharedManager]
                                attachedClients];
    self.client = [attachedClients firstObject];
    if (self.client) {
        [[MSBClientManager sharedManager] connectClient:self.client];
    }
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [MSBClientManager sharedManager].delegate = self;
    NSArray	*clients = [[MSBClientManager sharedManager] attachedClients];
    self.client = [clients firstObject];

    if (self.client == nil)
    {
        
        
        return;
    }
    
    [[MSBClientManager sharedManager] connectClient:self.client];
      }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)StartStopButton:(id)sender {
   
    __weak typeof(self) weakSelf = self;
    void (^handler)(MSBSensorAccelerometerData *, NSError *) = ^(MSBSensorAccelerometerData *accelerometerData, NSError *error)
    {
        weakSelf.accelLabel.text = [NSString stringWithFormat:@"X = %5.2f Y = %5.2f Z = %5.2f",
                                    accelerometerData.x,
                                    accelerometerData.y,
                                    accelerometerData.z];
       
    };
    
    NSError *stateError;
   
      self.view.backgroundColor = [UIColor redColor];
    //Stop Accel updates after 60 seconds
    [self performSelector:@selector(stopAccelUpdates) withObject:0 afterDelay:60];
}

@end
