//
//  AppDelegate.h
//  MicrosoftBandSensing
//
//  Created by Asad Malik on 10/3/16.
//  Copyright (c) 2016 GroupV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

