//
//  ViewController.h
//  MicrosoftBandSensing
//
//  Created by Asad Malik on 10/3/16.
//  Copyright (c) 2016 GroupV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MicrosoftBandKit_iOS/MicrosoftBandKit_iOS.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *txtOutput;
@property (weak, nonatomic) IBOutlet UILabel *accelLabel;
@property (weak, nonatomic) IBOutlet UIButton *StartStopRecording;

- (IBAction)StartStopButton:(id)sender;

@end

